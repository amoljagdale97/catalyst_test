from django.test import TestCase
from catalyst_test.company.models import Company


class CompanyTestCase(TestCase):
    def setUp(self):
        CompanyTestCase.objects.create(name="Neosoft", year_founded=2005.0,
                                       domain='information technology and services',
                                       )
        Animal.objects.create(name="cat", sound="meow")

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        lion = Animal.objects.get(name="lion")
        cat = Animal.objects.get(name="cat")
        self.assertEqual(lion.speak(), 'The lion says "roar"')
        self.assertEqual(cat.speak(), 'The cat says "meow"')
