from django.urls import path
from company.views import rest_apis_view, user_views, views

app_name = 'Company'

urlpatterns = [
    path('login/', views.login_view, name="login"),
    path('logout/', views.logout_view_new, name="logout"),
    path('query_page/', views.query_builder_view, name="query_page"),
    path('users/', user_views.user_list_view, name="user_list"),
    path('users/delete/<int:id>', user_views.user_delete_view, name="user_delete"),
    path('users/create', user_views.user_create, name="user_create"),
    path('api/query', rest_apis_view.CompanyQueryView.as_view(), name="api_query")

]
