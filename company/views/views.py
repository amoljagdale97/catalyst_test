from django.shortcuts import render, redirect
from django.contrib.auth import logout, authenticate, login
from django.contrib import messages
from company.models import Company
from django.contrib.auth.decorators import login_required


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('Company:query_page')
        else:
            messages.error(request, "Username password did not match!")
            return render(request, 'company/auth_pages/login.html')
    else:
        return render(request, 'company/auth_pages/login.html')


@login_required
def logout_view_new(request):
    logout(request)
    return redirect('Company:login')


@login_required
def query_builder_view(request):
    industry = Company.objects.order_by('industry').values_list('industry', flat=True).distinct()
    city = Company.objects.order_by('city').values_list('city', flat=True).distinct()
    state = Company.objects.order_by('state').values_list('state', flat=True).distinct()
    country = Company.objects.order_by('country').values_list('country', flat=True).distinct()
    year_founded = list(Company.objects.order_by('year_founded').values_list('year_founded', flat=True).distinct())
    year_founded.remove(None)
    year_founded = list(map(int, year_founded))
    data = {
        'industry': industry,
        'year_founded': year_founded,
        'state': state,
        'city': city,
        'country': country
    }
    return render(request, 'company/query_page.html', context=data)
