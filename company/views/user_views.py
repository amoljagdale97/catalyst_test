from django.shortcuts import render, redirect
from django.contrib.auth import logout as default_logout, authenticate, login
from django.contrib import messages
from django.contrib.auth import get_user_model
from company.forms import UserCreateForm
from django.contrib.auth.decorators import login_required


@login_required
def user_list_view(request):
    model_user = get_user_model()
    user_list = model_user.objects.all()
    return render(request, 'company/user_list_pages/user_list.html', context={'users': user_list})


@login_required
def user_delete_view(request, id):
    User = get_user_model()
    try:
        u = User.objects.get(id=id)
        u.delete()
        messages.success(request, "The user is deleted")
        return redirect("Company:user_list")
    except User.DoesNotExist:
        messages.error(request, "User doesnot exist")
        return redirect("Company:user_list")


@login_required
def user_create(request):
    if request.method == 'POST':
        form_user_create = UserCreateForm(request.POST)
        if form_user_create.is_valid():
            form_user_create.save()
            return redirect('Company:user_list')
    else:
        form_user_create = UserCreateForm()
    return render(request, 'company/user_list_pages/user_create.html', {"form1": form_user_create})
