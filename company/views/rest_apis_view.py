from rest_framework.views import APIView
from company.models import Company
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated


class CompanyQueryView(APIView):
    def get(self, request):
        data = request.GET
        data2 = {}
        for i in data:
            if i == 'name':
                data2['name__contains'] = data[i]
            elif i == 'employee_from':
                data2['current_employee_estimate__gte'] = int(data[i])
            elif i == 'employee_to':
                data2['current_employee_estimate__lte'] = int(data[i])
            else:
                data2[i] = data[i]

        print(data, data2)

        output_data = Company.objects.filter(**data2).count()
        return Response({"status": "success", "data": output_data}, status=status.HTTP_200_OK)


class CompanyIndustrySerializer(serializers.ModelSerializer):
    industry = serializers.CharField(max_length=255)

    class Meta:
        model = Company
        fields = ('industry',)
