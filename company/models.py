# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

#
# class Company(models.Model):
#     id = models.BigIntegerField(primary_key=True)
#     name = models.TextField(blank=True, null=True)
#     domain = models.TextField(blank=True, null=True)
#     year_founded = models.FloatField(db_column='year founded', blank=True, null=True)  # Field renamed to remove unsuitable characters.
#     industry = models.TextField(blank=True, null=True)
#     size_range = models.TextField(db_column='size range', blank=True, null=True)  # Field renamed to remove unsuitable characters.
#     locality = models.TextField(blank=True, null=True)
#     country = models.TextField(blank=True, null=True)
#     linkedin_url = models.TextField(db_column='linkedin url', blank=True, null=True)  # Field renamed to remove unsuitable characters.
#     current_employee_estimate = models.BigIntegerField(db_column='current employee estimate', blank=True, null=True)  # Field renamed to remove unsuitable characters.
#     total_employee_estimate = models.BigIntegerField(db_column='total employee estimate', blank=True, null=True)  # Field renamed to remove unsuitable characters.
#
#     class Meta:
#         db_table = 'company'

class Company(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.TextField(blank=True, null=True)
    domain = models.TextField(blank=True, null=True)
    year_founded = models.FloatField(db_column='year founded', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    industry = models.TextField(blank=True, null=True)
    size_range = models.TextField(db_column='size range', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    locality = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    linkedin_url = models.TextField(db_column='linkedin url', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    current_employee_estimate = models.BigIntegerField(db_column='current employee estimate', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    total_employee_estimate = models.BigIntegerField(db_column='total employee estimate', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    city = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    country1 = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'company4'
