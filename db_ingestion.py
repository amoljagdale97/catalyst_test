import pandas as pd
from sqlalchemy import create_engine
import time

engine = create_engine('postgresql://catalyst_user:catalyst_test@localhost:5432/postgres')
count = 1
chunksize = 10 ** 5
filename = "/home/webwerks/Downloads/catalyst_test_data/companies_sorted.csv"

for chunk in pd.read_csv(filename, chunksize=chunksize):
    print(count)
    start_time = time.time()
    chunk.set_index('id', inplace=True)
    chunk[['city', 'state', 'country1']] = chunk['locality'].str.split(', ', 2, expand=True)
    chunk.to_sql('company', engine, if_exists="append")
    end_time = time.time()
    print(start_time - end_time, ": time require")
    count += 1
