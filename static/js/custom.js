console.log("found")
function send_data(data){
    $.ajax({
        type: "GET",
        url: "/api/query",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            var x = document.getElementById("output");
            document.getElementById("output_text").innerHTML =data['data'];
            x.style.display = "block";

        },
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function collect_data(){
    var data ={}
    var ids = ['industry','country','state','city','employee_to','employee_from','year_founded']
    for (i=0;i<ids.length;i++){
        console.log(ids[i])
        var select = document.getElementById(ids[i]);
        var value = select.options[select.selectedIndex].value;
        if(value != ''){
            data[ids[i]] =value;
        }
    };
    data['name'] = document.getElementById('keyword').value;
    console.log(data)
    send_data(data)
}

function reset_input(){
    $('input').val('');
    $('select').prop('selectedIndex',0);
}