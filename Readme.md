**Catalyst Test** 

Imported company data by db_ingestion.py
```
python db_ingestion.py
```
Change filename and db details 
this script make changes in data locality get separated into 'city','state','country1'

- Secured Environment variables in .env file ,added .env in repo just for showcase

- Model of company created by 'inspectdb' django 

- '/catalyst_test/company/views' contains all views

- created django restframework for querying in db

- **NO API/View** can be access without login

- install required libraries
```commandline
 pip install -r requirement.txt
```
- Django all-auth configurations are done
- Query builder view page url 'query_page/'
![Scheme](repo_images_demo/query_builder1.png)
- Query builder api url 'api/query'
```
'http://127.0.0.1:8000/api/query?country=india&state=maharashtra&employee_to=300000&employee_from=0&name=india'
```
![Scheme](repo_images_demo/query_builder2.png)
- all the filter have distinct and sorted values from db in dropdown
- employee to and from have batches of 50,000 in drop down mapped to current employee estimate
- user view has list, create, delete functionality
![Scheme](repo_images_demo/users.png)